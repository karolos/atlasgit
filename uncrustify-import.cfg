#
# Style definition for C++ files. Based on 'Stroustrup' style (sort-of)
#
newlines                        = LF		# auto/lf/crlf/cr
indent_with_tabs		= 0		# 0=spaces only 1=indent to level only, 2=indent with tabs
input_tab_size			= 4		# original tab size
output_tab_size			= 4		# The size of tabs in the output (only used if align_with_tabs=true)
indent_columns			= 2		# The number of columns to indent per level. Usually 2,3,4 or 8.
indent_label			= 2		# pos: absolute col, neg: relative column
indent_align_string		= TRUE		# align broken strings
indent_brace			= 0		# Spaces to indent '{' from level
indent_namespace		= True		# Whether the 'namespace' body is indented
indent_extern                   = False		# Whether the 'extern "C"' body is indented
indent_class                    = True		# Whether the 'class' body is indented
indent_else_if                  = False		# False=treat 'else\nif' as 'else if' for indenting purposes; if true, indent the 'if' one level
indent_var_def_blk              = 0		# Amount to indent variable declarations after a open brace. neg=relative, pos=absolute

indent_constr_colon  = True # better alignment for people who prefer to put the constructor initializer colon on a separate line (FW)

# Put the * and & for pointer and refs directly after the type (FW)
sp_before_ptr_star               = remove
sp_after_ptr_star               = force
sp_before_byref                 = remove
sp_after_byref                  = force

# True:  indent continued function call parameters one indent level
# False: align parameters under the open paren
indent_func_call_param          = False		


indent_func_def_param           = False		# Same as indent_func_call_param, but for function defs
indent_func_proto_param         = False		# Same as indent_func_call_param, but for function protos
indent_func_class_param         = False		# Same as indent_func_call_param, but for class declarations


# The number of spaces to indent a continued '->' or '.'
# Usually set to 0, 1, or indent_columns.
indent_member                   = 1		

# If set, will indent trailing single line ('//') comments relative
# to the code instead of trying to keep the same absolute column
indent_relative_single_line_comments = True

# Spaces to indent 'case' from 'switch'
# Usually 0 or indent_columns.
indent_switch_case              = 0


nl_enum_brace			= remove	# "enum {" vs "enum \n {"
nl_union_brace			= remove	# "union {" vs "union \n {"
nl_struct_brace			= remove	# "struct {" vs "struct \n {"
nl_do_brace			= remove	# "do {" vs "do \n {"
nl_if_brace			= remove	# "if () {" vs "if () \n {"
nl_for_brace			= remove	# "for () {" vs "for () \n {"
nl_else_brace			= remove	# "else {" vs "else \n {"
nl_else_if                      = remove	# Add or remove newline between 'else' and 'if'
nl_while_brace			= remove	# "while () {" vs "while () \n {"
nl_switch_brace			= remove	# "switch () {" vs "switch () \n {"

# The number of blank lines after a block of variable definitions at the top of a function body
# 0 = No change (default)
nl_func_var_def_blk		= 1

# Whether to put a newline before 'case' statement, not after the first 'case'
nl_before_case			= True

nl_fcall_brace			= remove	# "foo() {" vs "foo()\n{"
nl_fdef_brace			= remove	# "int foo() {" vs "int foo()\n{"
nl_after_return			= TRUE		# Whether to put a blank line after 'return' statements, unless followed by a close brace.
nl_brace_while			= remove	# Add or remove newline between '}' and 'while' of 'do' statement
nl_brace_else			= remove	# Add or remove newline between '}' and 'else'

# Whether to not put blanks after '#ifxx', '#elxx', or before '#endif'. Does not affect the whole-file #ifdef.
nl_squeeze_ifdef		= TRUE		

nl_fdef_brace                   = remove	# Add or remove newline between function signature and '{' (ignore/add/remove/force)
nl_after_semicolon              = True		# Whether to put a newline after semicolons, except in 'for' statements

# Whether to put a newline after brace open.
# This also adds a newline before the matching brace close.
nl_after_brace_open             = True		

# The number of newlines before a 'private:', 'public:', 'protected:', 'signals:', or 'slots:' label.
# Will not change the newline count if after a brace open.
# 0 = No change.
nl_before_access_spec           = 2

# The number of newlines after a 'private:', 'public:', 'protected:', 'signals:' or 'slots:' label.
# 0 = No change.
# the option 'nl_after_access_spec' takes preference over 'nl_typedef_blk_start' and 'nl_var_def_blk_start'
nl_after_access_spec            = 1

# Add or remove unnecessary paren on 'return' statement
# mod_paren_on_return		= add		# "return 1;" vs "return (1);"

mod_full_brace_if		= ignore		# "if (a) a--;" vs "if (a) { a--; }"
mod_full_brace_for		= ignore		# "for () a--;" vs "for () { a--; }"
mod_full_brace_do		= ignore		# "do a--; while ();" vs "do { a--; } while ();"
mod_full_brace_while	= ignore		# "while (a) a--;" vs "while (a) { a--; }"

sp_before_semi			= remove	# Add or remove space before ';'. Default=Remove
sp_paren_paren			= remove	# space between (( and ))
sp_paren_brace                  = force		# Add or remove space between ')' and '{'
sp_sparen_brace                 = force		# Add or remove space between ')' and '{' of 'if', 'for', 'switch', and 'while', etc.
sp_return_paren			= remove	# "return (1);" vs "return(1);"
sp_sizeof_paren			= remove	# "sizeof (int)" vs "sizeof(int)"
sp_before_sparen		= force		# "if (" vs "if("
sp_after_sparen			= force		# "if () {" vs "if (){"
sp_after_cast			= force 	# "(int) a" vs "(int)a"
sp_inside_braces		= remove	# "{ 1 }" vs "{1}"
sp_inside_braces_struct		= remove	# "{ 1 }" vs "{1}"
sp_inside_braces_enum		= remove	# "{ 1 }" vs "{1}"
sp_inside_paren			= remove	# "( 1 )" vs "(1)"
sp_inside_fparen		= remove        # "( 1 )" vs "(1)" - functions
sp_inside_sparen		= remove        # "( 1 )" vs "(1)" - if/for/etc
sp_type_func			= force		# Add or remove space between return type and function name;minimum of 1 is forced except for pointer return types
sp_assign			= force		# Add or remove space around assignment operator '=', '+=', etc

# Add or remove space around arithmetic operator '+', '-', '/', '*', etc
# also '>>>' '<<' '>>' '%' '|'
sp_arith			= force		

sp_bool				= force
sp_compare			= force
sp_after_comma			= force
sp_func_def_paren		= remove 	# "int foo (){" vs "int foo(){"
sp_func_call_paren		= remove	# "foo (" vs "foo("
sp_func_proto_paren		= remove	# "int foo ();" vs "int foo();"
sp_paren_brace                  = force
sp_fparen_brace                 = force
sp_after_comma                  = force
sp_after_class_colon            = force
sp_before_class_colon           = remove
sp_after_operator               = force
sp_after_operator_sym           = force
sp_else_brace                   = force
sp_brace_else					= force

align_with_tabs			= FALSE		# use tabs to align
# align_on_tabstop		= FALSE		# align on tabstops
# align_enum_equ_span		= 4
# align_nl_cont			= TRUE
# align_var_def_span		= 2
# align_var_def_inline		= TRUE
# align_var_def_star		= TRUE
# align_var_def_colon		= TRUE
# align_assign_span		= 1
# align_struct_init_span		= 3
# align_var_struct_span		= 3
# align_right_cmt_span		= 3
# align_pp_define_span		= 3
# align_pp_define_gap		= 4
# align_number_left		= TRUE
# align_typedef_span		= 5
# align_typedef_gap		= 3

# cmt_star_cont			= TRUE

eat_blanks_before_close_brace	= TRUE
eat_blanks_after_open_brace	= TRUE

nl_start_of_file                = Remove
nl_end_of_file                  = Add
nl_after_func_body              = 2
nl_func_leave_one_liners        = True # We will leave one liners, sometimes these are quite valid... (FW)
nl_create_if_one_liner 			= True # Try to make onliners explicit...
#nl_after_func_body_one_liner    = 2
nl_before_access_spec           = 1
nl_func_type_name               = ignore

code_width                      = 120
cmt_width                       = 120
sp_cmt_cpp_start                = Add


# The number of newlines after a function prototype, if followed by another function prototype
nl_after_func_proto                       = 1        # number

# The number of newlines after a function prototype, if not followed by another function prototype
nl_after_func_proto_group                 = 1        # number

# Indent access specifiers; negative value is relative to previous column
indent_access_spec              = -2
